package screen;


import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.object.Background;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.SpriteGroup;
import com.golden.gamedev.object.background.ImageBackground;
import com.golden.gamedev.util.ImageUtil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

/**
 * Стартовый экран игры
 */
public class StartScreen extends GameObject {

    /**
     * Путь к фоновому изображению стартового экрана игры
     */
    private final static String SCREEN_BG = "assets/screens/start_screen.png";

    private final static int AVATAR_SIZE = 40;

    /**
     * Фон стартового экрана игры
     */
    private Background bg;
    private SpriteGroup avatars;
    private ArrayList<String> paths;

    /**
     * Конструктор класса с параметром
     *
     * @param gameEngine игровой движок
     */
    public StartScreen(GameEngine gameEngine) {
        super(gameEngine);
        bg = new ImageBackground(getImage(SCREEN_BG));
        this.setFPS(60);
    }

    /**
     * Инициализирует ресурсы
     */
    @Override
    public void initResources() {

        // Убрать курсор

        //hideCursor();
        avatars = new SpriteGroup("avatars");
        paths = new ArrayList<String>();
        File folder = new File("assets/sprites/player");
        Sprite tmp = null;
        if (folder.isDirectory()) {
            for (File f : folder.listFiles()) {
                if (f.isFile() && f.canRead()) {
                    tmp = new Sprite(ImageUtil.resize(getImage("assets/sprites/player/" + f.getName()),
                            AVATAR_SIZE, AVATAR_SIZE));
                    tmp.setLocation((20 + AVATAR_SIZE)*avatars.getSize() + 20, getHeight()/2);
                    tmp.setID(paths.size());
                    avatars.add(tmp);
                    paths.add("assets/sprites/player/" + f.getName());
                }
            }
        }
        avatars.setBackground(bg);
    }

    /**
     * Обновляет ресурсы
     *
     * @param elapsedTime время, прошедшее с момента последнего обновления
     */
    @Override
    public void update(long elapsedTime) {
        bg.update(elapsedTime);

        // Check click
        if (click()) {
            for (Sprite spr : avatars.getSprites()) {
                if (spr != null && checkPosMouse(spr, true)) {
                    view.ViewSetting.setPlayerSpriteImgPath(paths.get(spr.getID()));
                    // ... закрыть текущий экран
                    parent.nextGameID = 1;
                    finish();

                }
            }
        }
    }

    /**
     * Рендерит ресурсы
     *
     * @param g графический контекст
     */
    @Override
    public void render(Graphics2D g) {
        bg.render(g);
        avatars.render(g);
    }
}
