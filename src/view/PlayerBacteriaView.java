package view;

import com.golden.gamedev.util.ImageUtil;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Представление Бактерии игрока
 */
public class PlayerBacteriaView extends GameObjectView {


    /**
     * Конструктор класса
     *
     * @throws IOException по указанному пути не найден файл
     */
    public PlayerBacteriaView() throws IOException {

        // Загрузить изображение Бактерии игрока

        gameObjectImage = ImageIO.read(new File(ViewSetting.getPlayerSpriteImgPath()));
        gameObjectImage = ImageUtil.resize(gameObjectImage, 74, 74);
        // Установить загруженное изображение для спрайта Бактерии игрока

        gameObjectSprite.setImage(gameObjectImage);
    }

}
