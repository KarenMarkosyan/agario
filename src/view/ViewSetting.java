/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

/**
 *
 * @author godric
 */
public class ViewSetting {
    
    /**
     * Путь к изображению бактерии Игрока
     */
    private static String PLAYER_SPRITE_IMAGE_PATH = "assets/sprites/player/cell.png";
    
    public static String getPlayerSpriteImgPath() {
        return PLAYER_SPRITE_IMAGE_PATH;
    }
    
    public static void setPlayerSpriteImgPath(String img) {
        PLAYER_SPRITE_IMAGE_PATH = img;
    }
}
